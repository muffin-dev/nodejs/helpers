import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import { terser } from 'rollup-plugin-terser';

export default [
  {
    input: 'index.js',
    output:
    {
      name: 'JSHelpers',
      file: './js-helpers.min.js',
      format: 'iife',
      sourcemap: 'inline'
    },
    plugins: [
      resolve(),
      commonjs(),
      terser(),
    ],
  }
];
