/**
 * Waits the given amount of milliseconds before resolving the promise.
 * @param delay The time (in milliseconds) to wait.
 */
export function wait(delay: number): Promise<void> {
  return new Promise((resolve: () => void) => {
    setTimeout(() => {
      resolve();
    }, delay);
  });
}
