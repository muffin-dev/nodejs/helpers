export type TEventListener = (...params: unknown[]) => void;

/**
 * Native events manager.
 */
export class EventsManager {

  /**
   * @var _listeners The collection of listeners by events.
   */
  private _listeners = new Map<string | symbol, TEventListener[]>();

  /**
   * Adds a callback for the named event.
   * @param eventName The name of the event to listen.
   * @param listener The callback to add to the named event listeners.
   */
  public listen(eventName: string | symbol, listener: TEventListener): void {
    let listeners = this._listeners.get(eventName) || null;
    if (listeners === null) {
      listeners = new Array<TEventListener>();
      this._listeners.set(eventName, listeners);
    }
    listeners.push(listener);
  }

  /**
   * Alias of listen().
   * Adds a callback for the named event.
   * @param eventName The name of the event to listen.
   * @param listener The callback to add to the named event listeners.
   */
  public on = this.listen;

  /**
   * Removes a callback from the named event listeners list.
   * @param eventName The name of the event of which you want to remove the listener.
   * @param listener The callback you want to remove. Note that if you use anonymous methods, the listener won't be found in the list.
   * @returns Returns true if the callback exists in the named event's listeners list and has been removed successfully, otherwise false.
   */
  public removeListener(eventName: string | symbol, listener: TEventListener): boolean {
    if (this._listeners.has(eventName)) {
      const listeners = this._listeners.get(eventName);
      if (!listeners) {
        return true;
      }

      for (let i = 0; i < listeners.length; i++) {
        if (listeners[i] === listener) {
          listeners.splice(i, 1);
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Removes all listeners of the named event.
   * @param eventName The name of the event of which you want to remove the listeners.
   */
  public removeAllListeners(eventName: string | symbol): void {
    if (this._listeners.has(eventName)) {
      this._listeners.delete(eventName);
    }
  }

  /**
   * Triggers the named event, and call the registered listeners.
   * @param eventName The name of the event to trigger.
   * @param params The eventual data to send to the listeners.
   */
  public emit(eventName: string | symbol, ...params: unknown[]): void {
    const listeners = this._listeners.get(eventName);
    if (listeners) {
      for (const listener of listeners) {
        listener(...params);
      }
    }
  }

}
