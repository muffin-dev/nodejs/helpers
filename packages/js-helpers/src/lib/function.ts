/**
 * Converts the given method into an asynchronous method. If the input method is already asynchronous, this method won't apply any changes.
 * Note that inside the called method, "this" could be unexpected. This utility is meant to be used for methods that doesn't use "this", so
 * you have to pass any required data by the called method as a parameter.
 * @template TReturns The return type of the given method.
 * @param method The method to convert into an asynchronous method.
 * @param params The parameters to send of the converted method.
 * @returns Returns the converted method.
 */
export function asAsync<TReturns>(method: (...params: any[]) => TReturns|Promise<TReturns>, ...params: any[]): Promise<TReturns> {
  if (method.constructor.name === 'AsyncFunction') {
    return method(...params) as Promise<TReturns>;
  }
  else {
    return new Promise<TReturns>((resolve: (...params: any[]) => void) => {
      resolve(method(...params));
    });
  }
}

/**
 * Checks if the given method is a constructor.
 * IMPORTANT: In JavaScript, there's only one difference between a regular method an a constructor: a constructor is not callable. But this
 * is the case only when the class has at least one property. So be aware that this method is not magic, and won't work if the given method
 * is a constructor of a class that doesn't have any property.
 * @param func The method you want to check.
 * @returns Returns true if the given method is a constructor method for a class that has at least one property, otherwise false.
 */
export function isConstructor(func: unknown): boolean {
  if (typeof func !== 'function') {
    return false;
  }

  try { func(); }
  // If an error occurs when trying to call the method, it means that this method is a constructor
  catch { return true; }

  return false;
}
