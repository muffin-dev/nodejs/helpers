import { asArray } from './array';

/**
 * Overrides given original object's properties using matching override object's properties.
 * @param original The original object. It won't have more or less properties than before.
 * @param override The override object. Only its matching properties will override the original object. Other properties won't be
 * added to the original object.
 * @param properties Null by default. If given, the given properties are "whitelisted" (only the listed properties can be
 * overriden). If the array is null or empty, it won't filter properties.
 * @param propertiesAreIgnored If true, reverses the behavior of "properties" array, and given properties are "blacklisted" (all
 * properties but the listed one can be overriden).
 * @param recursive False by default. If true, if a property of the original is an object, it will call overrideObject() with the same
 * parameters (including whitelisted/blacklisted properties) on that property to merge the original and override properties. Otherwise, an
 * object property will just be replaced from the override to the original if it exists.
 * @returns Returns the original overriden object.
 */
export function overrideObject<T = unknown>(
  original: T,
  override: unknown,
  properties: string[] = null,
  propertiesAreIgnored = false,
  recursive = false
): T {
  if (!original || !override) {
    return original;
  }

  if (properties === null) {
    properties = [];
  }
  const typedOriginal = original as Record<string, unknown>;
  const typedOverride = override as Record<string, unknown>;

  // For each override object key
  for (const key of Object.keys(typedOverride)) {
    const index = (properties.length > 0) ? properties.indexOf(key) : -1;

    if (
      // If properties list is not used
      properties.length === 0 ||
      // OR if the properties array acts like a whitelist, and the current key is not listed
      (!propertiesAreIgnored && index !== -1) ||
      // OR if the properties array acts like a blacklist, and the current key is not listed
      (propertiesAreIgnored && index === -1)
    ) {
      // if the current key doesn't exist in the original object, skip it
      if (typedOriginal[key] === undefined) {
        continue;
      }

      // If that property is an object
      if (typeof typedOriginal[key] === 'object' && typedOriginal[key] !== null && !Array.isArray(typedOriginal[key])) {
        // If recursive call required
        if (recursive) {
          // Merge these objects
          typedOriginal[key] = overrideObject(typedOriginal[key], typedOverride[key], properties, propertiesAreIgnored);
        }
        // Else, replace the existing property of the original
        else {
          typedOriginal[key] = typedOverride[key];
        }
      }
      else {
        // Override original obejct's property
        typedOriginal[key] = typedOverride[key];
      }
    }
  }
  return original;
}

/**
 * Finds a property value on the given object, using a "property scheme".
 * @param obj The object that contains the target property. Assumes this object is valid.
 * @param propertyScheme A string formatted like 'propertyName' or 'myObj.propertyName' for example.
 * @param defaultValue Null by default. Defines the value to use if the named property is not found on the given object.
 * @returns Returns the value of the property targetted by the property scheme if it has been found, otherwise returns the default
 * value.
 */
export function findPropertyValue(obj: unknown, propertyScheme: string, defaultValue?: unknown): unknown {
  const splittedScheme = propertyScheme.split('.');
  const count = splittedScheme.length;
  if (count > 0) {
    let currentProperty = (obj as Record<string, unknown>)[splittedScheme[0]];
    for (let i = 1; i < count; i++) {
      if (currentProperty === undefined) {
        return defaultValue;
      }
      currentProperty = (currentProperty as Record<string, unknown>)[splittedScheme[i]];
    }
    return currentProperty;
  }
  return defaultValue;
}

/**
 * Makes a new object that contains only the named properties of the input object.
 * @param obj The object you want to extract the properties from.
 * @param properties The property or array of properties you want to extract from the input object.
 * @returns Returns the extracted object, or null if the input object is not valid.
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function extract<TOutput = any>(obj: unknown, properties: string|string[]): TOutput {
  if (!obj || typeof obj !== 'object') { return null; }
  properties = asArray(properties);
  const output: any = { };
  for (const property of properties) {
    if (!property || (obj as any)[property] === undefined) {
      continue;
    }
    output[property] = (obj as any)[property];
  }
  return output as TOutput;
}

/**
 * Creates a copy of the input object that omits the named properties or property schemes.
 * @param obj The object from which you want to omit properties.
 * @param properties The properties you want to omit. It can be property schemes e.g. "user.profile.name".
 * @returns Returns the processed object, or null if the given object is not valid.
 */
export function omit<TObject = Record<string, unknown>>(obj: Record<string, unknown>, properties: string|string[]): Partial<TObject> {
  // If the given object is not valid, return null
  if (!obj || typeof obj !== 'object') { return null; }

  obj = Object.assign({ }, obj);
  properties = asArray(properties);
  // For each property to omit
  for (const prop of properties) {
    // Split the property scheme
    const propertyPath = prop.split('.');
    let subObj = obj;
    // For each property of the scheme
    for (let i = 0; i < propertyPath.length; i++) {
      // If the current property is the last one
      if (i === propertyPath.length - 1) {
        // Delete the property from the object
        if (subObj[propertyPath[i]] !== undefined) {
          delete subObj[propertyPath[i]];
        }
        break;
      }
      // Else, select the sub object
      else {
        subObj = subObj[propertyPath[i]] as Record<string, unknown>;
        // If the sub object doesn't exist or is not an object, cancel
        if (subObj === undefined || typeof subObj !== 'object') {
          break;
        }
      }
    }
  }

  return obj as Partial<TObject>;
}

/**
 * Creates an object from the given array, by using its values as both keys and values of the output object.
 * @param arr The array you want to convert into an object.
 * @returns Returns the object, or null if the input data is not an array.
 */
export function fromArray<T extends string | number | symbol>(arr: T[]): Record<T, T> {
  if (!Array.isArray(arr)) {
    return null;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const obj: any = { };
  for (const item of arr) {
    if (item === null || item === undefined) {
      continue;
    }
    obj[item] = item;
  }
  return obj;
}
