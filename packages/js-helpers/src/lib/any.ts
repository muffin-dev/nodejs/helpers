/**
 * Converts the given value into a boolean.
 * Valid values: true, 1, "1", "true", "y", "yes", "enabled"
 * If the given value is a function, returns the result of asBoolean() on the returned value if that function. Note that this method is
 * synchronous, so it won't work with asynchronous methods.
 * @param value The value to convert into a boolean.
 * @returns Returns true if the given value is considered as valid, otherwise false.
 */
export function asBoolean(value: unknown): boolean {
  if (!value) {
    return false;
  }

  const type = typeof value;
  if (type === 'boolean') {
    return (value as boolean);
  }
  else if (type === 'string') {
    value = (value as string).trim().toLowerCase();
    return value === '1' || value === 'true' || value === 'y' || value === 'yes' || value === 'enabled';
  }
  else if (type === 'number') {
    return value === 1;
  }
  else if (type === 'function') {
    return asBoolean((value as () => boolean)());
  }
  return false;
}
