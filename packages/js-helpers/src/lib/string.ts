/**
 * Adds as many leading "0" as required before a given value. Note that if the input number length is higher than the maximum output string
 * length (defined with pow parameter), the output string will contain as many characters as the input number length.
 * Examples:
 *    - leading0(127, 3) = "127"
 *    - leading0(127, 5) = "00127"
 *    - leading0(127, 2) = "127"
 * @param value The input number.
 * @param pow Defines the maximum number size.
 * @return Returns the input number, preceded with as many leading 0 as required, or an empty string if the input value is not valid.
 */
export function leading0 (value: number, pow: number): string {
  if (typeof value !== 'number' || typeof value !== 'number') { return ''; }

  pow--;
  let valueString = '';
  let compNumber = Math.pow(10, pow);
  while (compNumber > 1) {
    if (value < compNumber) {
      valueString += '0';
    }
    pow--;
    compNumber = Math.pow(10, pow);
  }
  return valueString + value;
}

/**
 * Cut off the given text in several parts, each one having the given maximum length.
 * This method will first split your text by carriage returns, and try to fill the maximum possible space in each section with the splitted
 * content. If more granularity if needed, parts will be splitted by spaces. And if it's still not enough, characters will be splitted
 * separately.
 * @param content The content you want to cut off.
 * @param maxChars The maximum number of charcters in each section.
 * @returns Returns the cutted parts, or an empty array if the parameters are empty or not valid.
 */
export function cutoffText(content: string, maxChars: number): string[] {
  // If invalid arguments, returns an empty array
  if (typeof content !== 'string' || content === '' || typeof maxChars !== 'number' || maxChars <= 0) {
    return [];
  }

  // If the given content doesn't have to be cutted, returns it as the only array value
  if (content.length <= maxChars) {
    return [ content ];
  }

  const parts = new Array<string>();
  // Split the given content by carriage returns
  const splitReturns = content.split(/[\n\r]/);
  let messagePart = '';
  for (const part of splitReturns) {
    // If the current part itself is exceeding the characters limit, split by spaces
    if (part.length > maxChars) {
      // Split the current part by spaces
      const splitSpaces = part.split(/\s/);
      for (const partSpace of splitSpaces) {
        // If the current "part space" itself is exceeding the characters limit, split by spaces
        if (partSpace.length > maxChars) {
          // Splt the string by characters, and try to put the maximum of characters in each part
          if (messagePart !== '' && maxChars > 1 && (messagePart + ' ').length >= maxChars - 1) {
            parts.push(messagePart);
          }

          let startIndex = 0;
          let endIndex = maxChars;

          while (startIndex < partSpace.length) {
            parts.push(partSpace.slice(startIndex, endIndex));
            startIndex += maxChars;
            endIndex = Math.min(startIndex + maxChars, partSpace.length);
          }
        }

        // Else, if the current "part space" has less characters than the required maximum
        else {
          // If the current message is not empty, but will exceed the maximum number of characters if a space is added
          if (messagePart !== '' && (messagePart + ' ').length + partSpace.length > maxChars) {
            // Push the current message and replace it by the current part
            parts.push(messagePart);
            messagePart = partSpace;
          }
          // Else, if the current message is empty or can contain the current part
          else {
            // Add a space and the current part space
            if (messagePart !== '') {
              messagePart += ' ';
            }
            messagePart += partSpace;
          }
        }
      }
    }

    // Else, if the current part doesn't exceed the maximum number of chars
    else {
      // If the current message is not empty, but will exceed the maximum number of characters if a return is added
      if (messagePart !== '' && (messagePart + '\n').length + part.length > maxChars) {
        // Push the current message and replace it by the current part
        parts.push(messagePart);
        messagePart = part;
      }
      // Else, if the current message is empty or can contain the current part
      else {
        // Add a return character and the current part
        if (messagePart !== '') {
          messagePart += '\n';
        }
        messagePart += part;
      }
    }
  }

  // If a message is still in making, push it
  if (messagePart !== '') {
    parts.push(messagePart);
  }
  return parts;
}
