/**
 * Ensures the given value or values is an array, or convert it if needed.
 * @param value The value or array of values you want to convert into an array.
 * @template T Defines the type of the value.
 */
export function asArray<T>(value: T|T[]): T[] {
  return Array.isArray(value) ? value : [ value ];
}
