/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable unicorn/no-useless-undefined */
import assert from 'assert';
import { cutoffText, leading0 } from '../lib';

describe('Test overrideObject()', () => {
  it('should add 2 leading 0s', () => {
    assert.strictEqual(leading0(127, 5), '00127');
  });
  it('should not add leading 0s', () => {
    assert.strictEqual(leading0(127, 3), '127');
    assert.strictEqual(leading0(127, 2), '127');
  });
  it('should return empty string', () => {
    assert.strictEqual(leading0(null, 3), '');
    assert.strictEqual(leading0(null, null), '');
  });
});

describe('Test cutoffText()', () => {
  it('should not cut the text', () => {
    assert.deepStrictEqual(cutoffText('ABCD EFGH', 200), [ 'ABCD EFGH' ]);
  });
  it('should return the cutted text', () => {
    assert.deepStrictEqual(cutoffText('ABCDEFGH', 4), [ 'ABCD', 'EFGH' ]);
  });
  it('should return the cutted text', () => {
    assert.deepStrictEqual(cutoffText('ABCD EFGH', 4), [ 'ABCD', 'EFGH' ]);
  });
  it('should return the cutted text', () => {
    assert.deepStrictEqual(cutoffText('ABCD EFGH IJKL MNOP QRST UVWX YZ', 10), [ 'ABCD EFGH', 'IJKL MNOP', 'QRST UVWX', 'YZ' ]);
  });
  it('should return an empty array', () => {
    assert.deepStrictEqual(cutoffText(null, 10), []);
    assert.deepStrictEqual(cutoffText(undefined, 10), []);
  });
});
