import assert from 'assert';
import { asBoolean } from '../lib';

describe('Test asBoolean()', () => {
  it('should return true', () => {
    assert.strictEqual(asBoolean(true), true);
    assert.strictEqual(asBoolean('1'), true);
    assert.strictEqual(asBoolean('true'), true);
    assert.strictEqual(asBoolean('y'), true);
    assert.strictEqual(asBoolean('YES'), true);
    assert.strictEqual(asBoolean('  EnAbLeD  '), true);
    assert.strictEqual(asBoolean(1), true);
    assert.strictEqual(asBoolean(() => { return true; }), true);
    assert.strictEqual(asBoolean(() => { return '1'; }), true);
    // A method that returns a method that returns "yes"
    assert.strictEqual(asBoolean(() => {
      return () => { return 'yes'; };
    }), true);
  });

  it('should return false', () => {
    assert.strictEqual(asBoolean(null), false);
    // eslint-disable-next-line unicorn/no-useless-undefined
    assert.strictEqual(asBoolean(undefined), false);
    assert.strictEqual(asBoolean(0), false);
    assert.strictEqual(asBoolean(12), false);
    assert.strictEqual(asBoolean(21), false);
    assert.strictEqual(asBoolean({ value: true }), false);
    assert.strictEqual(asBoolean((): unknown => { return null; }), false);
    assert.strictEqual(asBoolean((): unknown => { return '12'; }), false);
    assert.strictEqual(asBoolean((): unknown => { return '21'; }), false);
  });
});
