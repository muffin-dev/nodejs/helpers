import assert from 'assert';
import { asArray } from '../lib';

describe('Test asArray()', () => {
  it('should return a string array', () => {
    assert.deepStrictEqual(asArray('test'), [ 'test' ]);
  });
  it('should return a boolean array', () => {
    assert.deepStrictEqual(asArray(true), [ true ]);
  });
  it('should return a null values array', () => {
    assert.deepStrictEqual(asArray(null), [ null ]);
  });
  it('should return an undefined values array', () => {
    // eslint-disable-next-line unicorn/no-useless-undefined
    assert.deepStrictEqual(asArray(undefined), [ undefined ]);
  });
  it('should return the same strings array', () => {
    assert.deepStrictEqual(asArray([ 'test' ]), [ 'test' ]);
  });
  it('should return the same booleans array', () => {
    assert.deepStrictEqual(asArray([ true ]), [ true ]);
  });
  it('should return the same strings array', () => {
    assert.deepStrictEqual(asArray([ 'a', 'b', 'c' ]), [ 'a', 'b', 'c' ]);
  });
  it('should return the same numbers array', () => {
    assert.deepStrictEqual(asArray([ 10, 20, 30 ]), [ 10, 20, 30 ]);
  });
});
