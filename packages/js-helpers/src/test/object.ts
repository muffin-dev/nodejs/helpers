/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable unicorn/no-useless-undefined */
import assert from 'assert';
import { extract, findPropertyValue, fromArray, omit, overrideObject } from '../lib';

describe('Test overrideObject()', () => {
  it('should override b', () => {
    const obj = { a: 0, b: 1 };
    const target = { a: 10, b: 11, c: 12 };
    assert.deepStrictEqual(overrideObject(obj, target), { a: 10, b: 11 });
  });
  it('should override a and c', () => {
    const obj = { a: 0, b: 1, c: 2 };
    const target = { a: 10, c: 12 };
    assert.deepStrictEqual(overrideObject(obj, target), { a: 10, b: 1, c: 12 });
  });
  it('should override nested properties', () => {
    const obj = { a: 0, b: { b1: 1, b2: 2 } };
    const target = { b: { b2: 10 } };
    assert.deepStrictEqual(overrideObject(obj, target, null, false, true), { a: 0, b: { b1: 1, b2: 10 } });
  });
  it('should not override nested properties', () => {
    const obj = { a: 0, b: { b1: 1, b2: 2 } };
    const target = { b: { b2: 10 } };
    assert.deepStrictEqual(overrideObject(obj, target), { a: 0, b: { b2: 10 } });
  });
  it('should only override mentionned properties', () => {
    const obj = { a: 0, b: { b1: 1, b2: 2 } };
    const target = { a: 10, b: { b1: 11, b2: 12 } };
    assert.deepStrictEqual(overrideObject(obj, target, [ 'a' ], false), { a: 10, b: { b1: 1, b2: 2 } });
  });
  it('should only override b1 property', () => {
    const obj = { a: 0, b: { b1: 1, b2: 2 } };
    const target = { a: 10, b: { b1: 11, b2: 12 } };
    assert.deepStrictEqual(overrideObject(obj, target, [ 'a' ], true), { a: 0, b: { b1: 11, b2: 12 } });
  });
  it('should do nothing', () => {
    assert.strictEqual(overrideObject(undefined, undefined), undefined);
  });
  it('should do nothing', () => {
    const obj = { a: 0, b: 1 };
    assert.deepStrictEqual(overrideObject(obj, true), { a: 0, b: 1 });
  });
  it('should do nothing', () => {
    const obj = { a: 0, b: 1 };
    assert.deepStrictEqual(overrideObject(obj, 'chips'), { a: 0, b: 1 });
  });
});

const testObj = {
  a: 0,
  b: {
    b1: 1,
    b2: 2
  }
};

describe('Test findPropertyValue()', () => {
  it('should return 0', () => {
    assert.strictEqual(findPropertyValue(testObj, 'a'), 0);
  });
  it('should return 2', () => {
    assert.strictEqual(findPropertyValue(testObj, 'b.b2'), 2);
  });
  it('should return b property', () => {
    assert.deepStrictEqual(findPropertyValue(testObj, 'b'), { b1: 1, b2: 2 });
  });
  it('should return undefined', () => {
    assert.strictEqual(findPropertyValue(testObj, 'c'), undefined);
  });
  it('should return null as default value', () => {
    assert.strictEqual(findPropertyValue(testObj, 'c.c1', null), null);
  });
});

describe('Test extract()', () => {
  it('should extract a', () => {
    assert.deepStrictEqual(extract(testObj, 'a'), { a: 0 });
  });
  it('should extract b', () => {
    assert.deepStrictEqual(extract(testObj, 'b'), { b: { b1: 1, b2: 2 } });
  });
  it('should extract a and b', () => {
    assert.deepStrictEqual(extract(testObj, [ 'a', 'b' ]), { a: 0, b: { b1: 1, b2: 2 } });
  });
  it('should extract nothing', () => {
    assert.deepStrictEqual(extract(testObj, undefined), { });
  });
  it('should return null', () => {
    assert.strictEqual(extract(undefined, 'a'), null);
  });
  it('should extract empty', () => {
    assert.deepStrictEqual(extract(testObj, 'c'), { });
  });
  it('should extract empty', () => {
    assert.deepStrictEqual(extract(testObj, [ 'c' ]), { });
  });
});

describe('Test omit()', () => {
  it('should omit a', () => {
    assert.deepStrictEqual(omit(testObj, 'a'), { b: { b1: 1, b2: 2 } });
  });
  it('should omit b', () => {
    assert.deepStrictEqual(omit(testObj, 'b'), { a: 0 });
  });
  it('should omit a and b', () => {
    assert.deepStrictEqual(omit(testObj, [ 'a', 'b' ]), { });
  });
  it('should omit nothing', () => {
    assert.deepStrictEqual(omit(testObj, [ 'c' ]), { a: 0, b: { b1: 1, b2: 2 } });
  });
});

describe('Test fromArray()', () => {
  it('should return array from strings', () => {
    assert.deepStrictEqual(fromArray([ 'a', 'b', 'c' ]), { a: 'a', b: 'b', c: 'c' });
  });
  it('should return array from numbers', () => {
    assert.deepStrictEqual(fromArray([ 0, 1, 2 ]), { 0: 0, 1: 1, 2: 2 });
  });
  it('should return null', () => {
    // @ts-ignore
    assert.strictEqual(fromArray('test'), null);
  });
  it('should return null', () => {
    // @ts-ignore
    assert.strictEqual(fromArray(undefined), null);
  });
  it('should return empty object', () => {
    // @ts-ignore
    assert.deepStrictEqual(fromArray([ undefined, null ]), { });
  });
});
