/* eslint-disable @typescript-eslint/ban-ts-comment */
import assert from 'assert';
import { isConstructor } from '../lib';

class Test {
  public a = 0;
}

function Test2() {
  // @ts-ignore
  this.a = 0;
}

describe('Test isConstructor()', () => {
  it('should return true', () => {
    // @ts-ignore
    assert.strictEqual(isConstructor(Test2), true);
  });
  it('should return true', () => {
    assert.strictEqual(isConstructor(Test), true);
  });
  it('should return true', () => {
    assert.strictEqual(isConstructor(new Test().constructor), true);
  });
  it('should return true', () => {
    // @ts-ignore
    assert.strictEqual(isConstructor(new Test2().constructor), true);
  });
  it('should return false', () => {
    assert.strictEqual(isConstructor(null), false);
  });
  it('should return false', () => {
    // eslint-disable-next-line unicorn/no-useless-undefined
    assert.strictEqual(isConstructor(undefined), false);
  });
  it('should return false', () => {
    assert.strictEqual(isConstructor(true), false);
  });
  it('should return false', () => {
    assert.strictEqual(isConstructor({ a: '0' }.constructor), false);
  });
});
