# Muffin Dev for Node - JS Helpers - `any` extensions

## Methods

### `asBoolean()`

```js
function asBoolean(value);
```

Converts the given value into a boolean.

The `true` values are: `true`, `1`, `"1"`, `"t"`, `"true"`, `"y"` and `"yes"`.

If the given value is a function, returns the result of `asBoolean()` on the returned value if that function. Note that this method is synchronous, so it won't work with asynchronous methods.

- `value: any`: The value to convert into a boolean

Returns `true` if the given value is considered as valid, otherwise `false`.