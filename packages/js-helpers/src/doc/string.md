# Muffin Dev for Node - JS Helpers - `string` extensions

## Methods

### `leading0()`

```js
function leading0(value, pow);
```

Adds as many leading "0" as required before a given value.

Note that if the input number length is higher than the maximum output string length (defined with pow parameter), the output string will contain as many characters as the input number length.

- `value: number`: The input number
- `pow: number`: Defines the maximum number size

Returns the input number, preceded with as many leading 0 as required.

Example:

```js
const leading0 = require('@muffin-dev/js-helpers').leading0;
console.log(leading0(127, 3)); // Outputs "127"
console.log(leading0(127, 5)); // Outputs "00127"
console.log(leading0(127, 2)); // Outputs "127"
console.log(leading0(127.38, 4)); // Outputs "0127.38"
```

### `cutoffText()`

```js
function cutoffText(content, maxChars);
```

- `content: string`: The content you want to cut off.
- `maxChars: number`: The maximum number of charcters in each section.

Cut off the given text in several parts, each one having the given maximum length.

This method will first split your text by carriage returns, and try to fill the maximum possible space in each section with the splitted content. If more granularity if needed, parts will be splitted by spaces. And if it's still not enough, characters will be splitted separately.

Returns the cutted parts, or an empty array if the parameters are empty or not valid.

Example:

```js
const cutoffText = require('@muffin-dev/js-helpers').cutoffText;

const STRING = 'ABCD EFGH IJKL MNOP QRST UVWX YZ';
console.log('cutoffText (max. chars 100)', cutoffText(STRING, 100));
console.log('cutoffText (max. chars 10)', cutoffText(STRING, 10));
console.log('cutoffText (max. chars 7)', cutoffText(STRING, 7));
console.log('cutoffText (max. chars 1)', cutoffText(STRING, 1));

// Output:
// "cutoffText (max. chars 100)" [ 'ABCD EFGH IJKL MNOP QRST UVWX YZ' ]
// "cutoffText (max. chars 10)" [ 'ABCD EFGH', 'IJKL MNOP', 'QRST UVWX', 'YZ' ]
// "cutoffText (max. chars 7)" [ 'ABCD', 'EFGH', 'IJKL', 'MNOP', 'QRST', 'UVWX YZ' ]
// "cutoffText (max. chars 1)" [
//   'A', 'B', 'C', 'D', 'E', 'F',
//   'G', 'H', 'I', 'J', 'K', 'L',
//   'M', 'N', 'O', 'P', 'Q', 'R',
//   'S', 'T', 'U', 'V', 'W', 'X',
//   'Y', 'Z'
// ]
```
