# Muffin Dev for Node - JS Helpers - `Function` extensions

## Methods

### `asAsync()`

```js
function asAsync(method, ...params)
```

Converts the given method into an asynchronous method. If the input method is already asynchronous, this method won't apply any changes.

- `TReturns` template: The return type of the given method
- `method: (...params: any[]) => TReturns|Promise<TReturns>`: The method to convert into an asynchronous method
- `...params: any[]`: The parameters to send of the converted method

Returns the converted method.

Example:

```js
const asAsync = require('@muffin-dev/js-helpers').asAsync;
const wait = require('@muffin-dev/js-helpers').wait;

function syncTest() {
    console.log('Synchronous test');
}

function syncTestWithParams(name, score) {
    console.log('Synchronous test with params', { name, score });
}

async function asyncTest() {
    await wait(1000);
    console.log('Asynchronous test');
}

async function asyncTestWithParams(name, score) {
    await wait(1000);
    console.log('Asynchronous test with params', { name, score });
}

(async () => {
    // Use asAsync() to create an array of promises
    const processes = [];
    processes.push(asAsync(syncTest));
    processes.push(asAsync(syncTestWithParams, 'User1', 80));
    processes.push(asAsync(asyncTest));
    processes.push(asAsync(asyncTestWithParams, 'User2', 120));

    // Run all the promises simultaneously
    await Promise.all(processes);
    // Outputs only after all the test methods have been called
    console.log('All processes have been executed!');
});
```

Relative content:

- [`wait()` method](./utilities.md)

### `isConstructor()`

```js
function isConstructor(func)
```

Checks if the given method is a constructor.

***IMPORTANT*: In JavaScript, there's only one difference between a regular method an a constructor: a constructor is not callable. But this is the case only when the class has at least one property. So be aware that this method is not magic, and won't work if the given method is a constructor of a class that doesn't have any property.**

- `func: unknown`: The method you want to check

Returns `true` if the given method is a constructor method for a class that has at least one property, otherwise `false`.