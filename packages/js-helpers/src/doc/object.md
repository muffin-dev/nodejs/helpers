# Muffin Dev for Node - JS Helpers - `object` extensions

## Methods

### `overrideobject()`

```js
function overrideobject(original, override, properties = null, propertiesAreIgnored = false, recursive = false);
```

Overrides the given original object's properties using the matching override object's properties.

- `original: object`: 
- `override: object`: 
- `properties: string[] = null`: If given, the given properties are "whitelisted" (only the listed properties will be overriden). If the array is null or empty, it won't filter properties
- `propertiesAreIgnored: boolean = false`: If `true`, reverses the behavior of *properties* array, and given properties are "blacklisted" (all properties but the listed one will be overriden)
- `recursive: boolean = false`: If true, if a property of the original is an object, it will call `overrideObject()` with the same parameters (includineg whitelisted/blacklisted properties) on that property to merge the original and override properties. Otherwise, an object property will just be replaced from the override to the original if it exists.

Returns the overriden original object. Note that the input object is modified in-place.

### `findPropertyValue()`

```js
function findPropertyValue(obj, propertyScheme, defaultValue);
```

Finds a property value on the given object, using a "property scheme".

- `obj: any`: The object that contains the target property. Assumes this object is valid
- `propertyScheme: string`: A string formatted like 'propertyName' or 'myObj.propertyName' for example
- `defaultValue: any = null`: Defines the value to use if the named property is not found on the given object

Returns the value of the property targetted by the property scheme if it has been found, otherwise returns the default value.

Example:

```ts
const findPropertyValue = require('@muffin-dev/js-helpers');
const demoObject = {
    name: 'Batman,
    infos: {
        description: 'I\'m Batman.',
        superpowers: {
            day: 'Sleeping rich man',
            night: 'I\'m Batman'
        }
    }
};
console.log(findPropertyValue(demoObject, 'infos.superpowers.day'));
// Outputs "Sleeping rich man"
```

### `extract()`

```js
function extract(obj, properties);
```

Makes a new object that contains only the named properties of the input object.

- template `TOutput`: Defines the type of the output object.
- `obj: any`: The object you want to extract the properties from.
- `properties: string | string[]`: The property or array of properties you want to extract from the input object.

Returns the extracted object, or null if the input object is not valid.

Example:

```ts
const extract = require('@muffin-dev/js-helpers').extract;
const inputObject = {
    name: 'AAA',
    highscore: 251500,
    level: 17,
};
// Outputs { name: "AAA", highscore, 251500 }
console.log(extract(inputObject, [ 'name', 'highscore' ]));
```

### `omit()`

```ts
function omit(obj, properties)
```

Creates a copy of the input object that omits the named properties or property schemes.

- `obj: Record<string, unknown>`: The object from which you want to omit properties
- `properties: string|string[]`: The properties you want to omit. It can be property schemes e.g. `user.profile.name`

Returns the processed object, or null if the given object is not valid.

Example:

```ts
const omit = require('@muffin-dev/js-helpers').omit;
const obj = {
    a: 1,
    b: 2,
    c: {
        c1: 3,
        c2: 4
    }
};
// Outputs { a: 1, c: { c2: 4 } }
console.log(omit(obj, ['b', 'c.c1']));
```

### `fromArray()`

/**
 * Creates an object from the given array, by using its values as both keys and values of the output object.
 * @param arr The array you want to convert into an object.
 * @returns Returns the object, or null if the input data is not an array.
 */

```ts
function fromArray(arr)
```

Creates an object from the given array, by using its values as both keys and values of the output object.

- `arr: string[] | number[]`: The array to convert into an object.

Returns the object, or `null` if the input data is not an array.

Example:

```js
const fromArray = require('@muffin-dev/js-helpers').fromArray();

const fruits: [ 'apple', 'banana', 'lemon' ];
const fruitsObject = fromArray(fruits);

// Outputs { apple: 'apple', banana: 'banana', lemon: 'lemon' }
console.log(fruitsObject);
```