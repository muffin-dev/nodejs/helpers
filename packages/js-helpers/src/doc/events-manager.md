# Muffin Dev for Node - JS Helpers - Native events manager

Provides an `EventsManager` class that allow you to create an events manager just like the Node JS' `EventEmitter`, but with vanilla JS.

## Public API

### Methods

#### `listen()`

```js
listen(eventName, listener)
```

Adds a callback for the named event.

- `eventName: string | symbol`: The name of the event to listen.
- `listener: TEventListener`: The callback to add to the named event listeners.

#### `on()`

```js
on(eventName, listener)
```

Alias of `listen()`. Adds a callback for the named event.

- `eventName: string | symbol`: The name of the event to listen.
- `listener: TEventListener`: The callback to add to the named event listeners.

#### `removeListener()`

```js
removeListener(eventName, listener)
```

Removes a callback from the named event listeners list.

- `eventName: string | symbol`: The name of the event of which you want to remove the listener.
- `listener: TEventListener`: The callback you want to remove. Note that if you use anonymous methods, the listener won't be found in the list.

Returns `true` if the callback exists in the named event's listeners list and has been removed successfully, otherwise `false`.

#### `removeAllListeners()`

```js
removeAllListeners(eventName)
```

Removes all listeners of the named event.

- `eventName: string | symbol`: The name of the event of which you want to remove the listeners.

#### `emit()`

```js
emit(eventName, ...params)
```

Triggers the named event, and call the registered listeners

- `eventName: string | symbol`: The name of the event to trigger.
- `...params: unknown[]`: The eventual data to send to the listeners.