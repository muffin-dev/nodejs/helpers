# Muffin Dev for Node - JS Helpers - Utilities

## Methods

### `wait()`

```js
function wait(delay: number): Promise<void>
```

Waits the given amount of milliseconds before resolving the promise.

- `delay: number`: The time (in milliseconds) to wait

Example:

```js
const wait = require('@muffin-dev/js-helpers').wait;

(async () => {
    console.log('Wait a second...');
    await wait(1000);
    console.log('Done!');
})();
```