# Muffin Dev for Node - JS Helpers - `Array` extensions

## Methods

### `asArray()`

```js
function asArray(value)
```

Ensures the given value or values is an array, or convert it if needed.

- `T` template: The type of the value
- `value: T|T[]`: The time (in milliseconds) to wait

Returns a new array with the given value as the only item, otherwise returns the input value if it's already an array.

Example:

```js
const asArray = require('@muffin-dev/js-helpers').asArray;

console.log('To array:', asArray('test')); // Ouputs [ 'test' ]
console.log('To array:', asArray([ 'A', 'B', 'C' ])); // The input value is already an array, so it outputs [ 'A', 'B', 'C' ]
```
