# Muffin Dev for Node - JS Helpers

This module contains a set of utilities for JavaScript.

## Installation

### In browser

Just download the `js-helpers.min.js` file, and import it in your we pages:

```html
<script src="js-helpers.min.js"></script>
```

### With Node JS

Install it locally with NPM by using the following command:

```bash
npm i @muffin-dev/js-helpers
```

## Usage

### In browser

Import the library in your web page with the `<script>` tag, and use the `JSHelpers` global variable to use the library:

```html
<script src="js-helpers.min.js"></script>
<script>
    // Will log "00127" in your browser console
    console.log(JSHelpers.leading0(127, 5));
</script>
```

### With Node JS

```js
const JSHelpers = require('@muffin-dev/js-helpers');
// OR, if you need only a specific method
// const leading0 = require('@muffin-dev/js-helpers').leading0;

// Will log "00127" in your terminal
console.log(JSHelpers.leading0(127, 5));
```

## Documentation

### Tools

- [Native events manager](./events-manager.md)

### Helpers

- [`any` extensions](./any.md)
- [`array` extensions](./array.md)
- [`function` extensions](./function.md)
- [`object` extensions](./object.md)
- [`string` extensions](./string.md)
- [Other helpers](./utilities.md)