import assert from 'assert';
import Path from 'path';
import { readdirAsync } from '../lib';

describe('Test readdirAsync()', () => {
  it('should only get all files', async () => {
    const files = await readdirAsync(Path.join(__dirname, './test-dir'), false, true, null, false, true);
    assert.strictEqual(files.length, 3);
  });

  it('should only get *.js file', async () => {
    const files = await readdirAsync(Path.join(__dirname, './test-dir'), false, true, 'js', false, true);
    assert.strictEqual(files.length, 1);
    assert.strictEqual(files[0].extension, 'js');
  });

  it('should only get *.js and *.less files', async () => {
    const files = await readdirAsync(Path.join(__dirname, './test-dir'), false, true, [ 'js', 'less' ], false, true);
    assert.strictEqual(files.length, 2);
    assert.strictEqual(files[0].extension, 'js');
    assert.strictEqual(files[1].extension, 'less');
  });

  it('should get all files but *.js ones', async () => {
    const files = await readdirAsync(Path.join(__dirname, './test-dir'), false, true, 'js', false, true, true);
    assert.strictEqual(files.length, 2);
    assert.strictEqual(files[0].extension, 'less');
    assert.strictEqual(files[1].extension, 'txt');
  });

  it('should get all files but *.js and *.less ones', async () => {
    const files = await readdirAsync(Path.join(__dirname, './test-dir'), false, true, [ 'js', 'less' ], false, true, true);
    assert.strictEqual(files.length, 1);
    assert.strictEqual(files[0].extension, 'txt');
  });
});
