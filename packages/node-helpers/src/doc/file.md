# Muffin Dev for Node - Node Helpers - API Documentation - File utilities

## `readFileAsync`

```ts
async function readFileAsync(path: string, options: TReadFileOptions = null, ignoreMissingFile = false): Promise<any>
```

Reads a file asynchronously. Note that it uses NodeJS's FileSystem.readFile() method, but transforming the callback into a promise.

- `path: string`: Path to the file.
- `options: TReadFileOptions = null`: If set, defines the data encoding. If null given, uses "utf8" by default. For more informations, see the documentation of FileSystem.readFile() method on the [Node JS official documentation](https://nodejs.org/dist/latest-v14.x/docs/api/fs).
- `ignoreMissingFile = false`: If true, returns null instead of throwing an error if the file at the given path doesn't exist.

Returns the file content if the file have been read successfully.

Relative content:

- [`TReadFileOptions` type documentation](./types.md)

## `readdirAsync`

```ts
async function readdirAsync(
  path: string,
  includeDirectories: boolean = false,
  includeFiles: boolean = true,
  extensions: string | string[] | null = null,
  ignoreMissingFolder: boolean = false,
  recursive: boolean = false,
  ignoreExtensions = false
): Promise<IDirItem[]>
```

Read the content of a directory.

- `path: string`: Path to the directory to read
- `includeDirectories: boolean = false`: Defines if directories must be queried
- `includeFiles: boolean = true`: Defines if files must be queried
- `extensions: string | string[] = null`: If includeFiles parameter is set to true, and this value is not null (could be a string or an array of strings), filters only files with the specified extension (or one of them if an array is passed). Note that extensions passed must not contain "."
- `ignoreMissingFolder: boolean = false`: If true, returns an empty array instead of throwing an error if the folder at given path doesn't exist.
- `recursive: boolean = false`: If true, gets recursively files and folders into folders
- `ignoreExtensions = false`: Reverse the "extensions" parameter behavior. If true and the extensions parameter is not null, include all found files but the one with the named extensions.

Returns an array of all the found files and directories.

Relative content:

- [`IDirItem` interface documentation](./interfaces.md)

## `readJSONFileAsync`

```ts
async function readJSONFileAsync(path: string, options: TReadFileOption = null, ignoreMissingFile: boolean = false): Promise<any | null>
```

- `path: string`: Path to the JSON file
- `options: TReadFileOptions = null`: If set, defines the data encoding. If null, uses "utf8" by default. For more informations, see the documentation of FileSystem.readFile() method on the Node JS official documentation: https://nodejs.org/dist/latest-v14.x/docs/api/fs.html#fs_fs_readfile_path_options_callback
- `ignoreMissingFile: boolean = false`: If true, returns an empty object instead of throwing an error if the file doesn't exist

Reads a JSON file asynchronously.

Relative content:

- [`TReadFileOptions` type documentation](./types.md)

## `writeFileAsync`

```ts
async function writeFileAsync(path: string, data: any, options: FileSystem.WriteFileOptions = null): Promise<void>
```

Writes a file asynchronously. Do the same as fs.writeFile(), but creates folders to the given path if they don't exist.

- `path: string`: Path to the file to write
- `data: any`: Data to write into the file
- `options: FileSystem.WriteFileOptions = null`: Options for the file to write. If null given, uses "utf8" by default

## `copyFileAsync`

```ts
async function copyFileAsync(source: string, target: string): Promise<void>
```

Copies a file asynchronously. Do the same as fs.copyFile(), but creates the missing directories to the given target if they don't exist.

- `source: string`: Path to the source file to copy
- `target: string`: Path to the copy destination

## `copyDirectory`

```ts
async function copyDirectory(source: string, target: string): Promise<void>
```

Copies an entire directory from given source path to given target path. Note that target folders are created if they don't exist.

- `source: string`: Path to the original directory to copy
- `target: string`: Path to the copied files destination

## `removeFile`

```ts
async function removeFile(path: string, )
```

Removes a file at the given path asynchronously.

- `path: string`: Path to the file to remove
- `ignoreMissingFile: boolean = false`: If `true`, this method won't throw error if the file doesn't exist