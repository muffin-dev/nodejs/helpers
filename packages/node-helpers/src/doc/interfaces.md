# Muffin Dev for Node - Node Helpers - API Documentation - Interfaces

## `IDirItem`

Contains data of a folder or a file such as path and name.

Implementation:

```ts
interface IDirItem {
   // Is this item a file?
  isFile: boolean;

  // Is this item a directory?
  isDirectory: boolean;

  // Name of the file (without extension) or the directory.
  name: string;

  // Name of the file (including extension) or the directory.
  fullName: string;

  // Extension of the file. If the item is a directory, this is null.
  extension?: string | null;

  // Relative path to the file or the directory.
  path: string;
}
```