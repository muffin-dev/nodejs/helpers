# Muffin Dev for Node - Node Helpers - API Documentation - Types

## `TReadFileOptions`

```ts
type TReadFileOptions = string | { encoding: BufferEncoding, flag?: string };
```

Represents the options that can be passed for reading a file. This type is used by methods that allows you to read a file. These methods are based on the Node JS [`FileSystem.readFile()`](./file.md) method. For more informations about these parameters, [see the official documentation](https://nodejs.org/dist/latest-v14.x/docs/api/fs.html#fs_fs_readfile_path_options_callback).