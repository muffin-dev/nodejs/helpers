import Path from 'path';

/**
 * Converts the given path into an absolute path.
 * @param path The path you want to convert to an absolue path. If the given path is already absolute, this methods doesn't make any
 * change.
 * @param absOrigin The absolute path you want to resolve the given path from. If null given, resolves the path from process.cwd().
 * @returns Returns the resolved path, or the input path if it was already absolute.
 */
export function asAbsolute(path: string, absOrigin: string = null): string {
  if (!Path.isAbsolute(path)) {
    if (!absOrigin) {
      absOrigin = process.cwd();
    }
    path = Path.join(absOrigin, path);
  }
  return path;
}
